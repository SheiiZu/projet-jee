# Projet JEE

Inscription à des cours de soutien

Projet pédagogique 2ième année d'école d'ingénieurs (I2FA) - 3iL ingénieurs Limoges France

# Technologies employées

- JSF
- JPA
- EJB
- MapStruct
- Maven


L’application suit une architecture multicouche, incluant notamment une couche Service et une couche DAO.

Les classes représentant les données sont distinctes selon qu’on est dans la couche UI ou dans la couche Logique Métier. 

Ces 2 couches communiquent en utilisant des classes DTO. 

La correspondance entre les différentes classes est faite en utilisant un objet mapper basé sur MapStruct.

L’implémentation est faite sous la forme de plusieurs modules, notamment un module web et un module EJB distincts.

# Spécifications
[Cliquez ici](https://moodle.3il.fr/mod/resource/view.php?id=28857)

# Schéma initial imaginé en réponse au sujet
![Schéma initial](https://i.imgur.com/sNup1ds.png)
