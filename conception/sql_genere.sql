-- Non compatible avec postgresql tel quel

CREATE TABLE Enfant(
   Id_Enfant COUNTER,
   nom VARCHAR(50),
   prenom VARCHAR(50),
   dateNaissance DATE,
   etablissement VARCHAR(50),
   PRIMARY KEY(Id_Enfant)
);

CREATE TABLE Mouvement(
   Id_Mouvement COUNTER,
   montant DECIMAL(15,2) NOT NULL,
   dateMvmtCompteAsso DATE NOT NULL,
   PRIMARY KEY(Id_Mouvement)
);

CREATE TABLE Utilisateur(
   Id_Utilisateur COUNTER,
   nom VARCHAR(50),
   prenom VARCHAR(50),
   mail VARCHAR(50),
   motDePasse VARCHAR(50),
   modePaiement VARCHAR(50),
   nombrePaiement VARCHAR(50),
   restantAPayer VARCHAR(50),
   Id_Mouvement INT NOT NULL,
   PRIMARY KEY(Id_Utilisateur),
   FOREIGN KEY(Id_Mouvement) REFERENCES Mouvement(Id_Mouvement)
);

CREATE TABLE Parent(
   Id_Parent COUNTER,
   Id_Enfant INT NOT NULL,
   Id_Utilisateur INT NOT NULL,
   PRIMARY KEY(Id_Parent),
   UNIQUE(Id_Utilisateur),
   FOREIGN KEY(Id_Enfant) REFERENCES Enfant(Id_Enfant),
   FOREIGN KEY(Id_Utilisateur) REFERENCES Utilisateur(Id_Utilisateur)
);

CREATE TABLE Gestionnaire(
   Id_Gestionnaire COUNTER,
   Id_Utilisateur INT NOT NULL,
   PRIMARY KEY(Id_Gestionnaire),
   UNIQUE(Id_Utilisateur),
   FOREIGN KEY(Id_Utilisateur) REFERENCES Utilisateur(Id_Utilisateur)
);

CREATE TABLE Cours(
   Id_Cours COUNTER,
   libelle VARCHAR(50),
   dateHeureSeance DATETIME,
   salle VARCHAR(50),
   Id_Parent INT NOT NULL,
   PRIMARY KEY(Id_Cours),
   FOREIGN KEY(Id_Parent) REFERENCES Parent(Id_Parent)
);

CREATE TABLE Niveau(
   Id_Niveau COUNTER,
   libelle VARCHAR(50),
   Id_Cours INT NOT NULL,
   PRIMARY KEY(Id_Niveau),
   FOREIGN KEY(Id_Cours) REFERENCES Cours(Id_Cours)
);

CREATE TABLE est_attribué(
   Id_Enfant INT,
   Id_Niveau INT,
   PRIMARY KEY(Id_Enfant, Id_Niveau),
   FOREIGN KEY(Id_Enfant) REFERENCES Enfant(Id_Enfant),
   FOREIGN KEY(Id_Niveau) REFERENCES Niveau(Id_Niveau)
);
