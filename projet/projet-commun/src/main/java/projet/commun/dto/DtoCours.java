package projet.commun.dto;

import java.io.Serializable;
import java.time.LocalDateTime;

@SuppressWarnings("serial")
public class DtoCours implements Serializable {

	private int id;

	private int id_parent;

	private String libelle;

	private LocalDateTime dateHeureSeance;

	private String salle;

	public DtoCours() {
		super();
	}

	public DtoCours(int id, int id_parent, String libelle, LocalDateTime dateHeureSeance, String salle) {
		super();
		this.id = id;
		this.id_parent = id_parent;
		this.libelle = libelle;
		this.dateHeureSeance = dateHeureSeance;
		this.salle = salle;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_parent() {
		return id_parent;
	}

	public void setId_parent(int id_parent) {
		this.id_parent = id_parent;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public LocalDateTime getDateHeureSeance() {
		return dateHeureSeance;
	}

	public void setDateHeureSeance(LocalDateTime dateHeureSeance) {
		this.dateHeureSeance = dateHeureSeance;
	}

	public String getSalle() {
		return salle;
	}

	public void setSalle(String salle) {
		this.salle = salle;
	}
}
