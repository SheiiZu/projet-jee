package projet.commun.dto;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class DtoEnfant implements Serializable {

	// -------
	// Champs
	// -------

	private Date dateNaissance;

	private String etablissement;

	private int id;

	private String nom;

	private String prenom;

	public DtoEnfant() {
		super();
	}

	public DtoEnfant(Date dateNaissance, String etablissement, int id, String nom, String prenom) {
		super();
		this.dateNaissance = dateNaissance;
		this.etablissement = etablissement;
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getEtablissement() {
		return etablissement;
	}

	public void setEtablissement(String etablissement) {
		this.etablissement = etablissement;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}
