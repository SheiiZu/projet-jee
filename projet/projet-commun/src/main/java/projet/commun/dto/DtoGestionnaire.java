package projet.commun.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class DtoGestionnaire implements Serializable {

	private int id;

	private int id_utilisateur;

	public DtoGestionnaire() {
		super();
	}

	public DtoGestionnaire(int id, int id_utilisateur) {
		super();
		this.id = id;
		this.id_utilisateur = id_utilisateur;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_utilisateur() {
		return id_utilisateur;
	}

	public void setId_utilisateur(int id_utilisateur) {
		this.id_utilisateur = id_utilisateur;
	}
}
