package projet.commun.dto;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class DtoMouvement implements Serializable {

	private int id;

	private Date dateMvmtCompteAsso;

	public DtoMouvement() {
		super();
	}

	public DtoMouvement(int id, Date dateMvmtCompteAsso) {
		super();
		this.id = id;
		this.dateMvmtCompteAsso = dateMvmtCompteAsso;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateMvmtCompteAsso() {
		return dateMvmtCompteAsso;
	}

	public void setDateMvmtCompteAsso(Date dateMvmtCompteAsso) {
		this.dateMvmtCompteAsso = dateMvmtCompteAsso;
	}
}
