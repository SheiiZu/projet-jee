package projet.commun.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class DtoParent implements Serializable {

	private int id;

	private int id_enfant;

	private int id_utilisateur;

	public DtoParent() {
		super();
	}

	public DtoParent(int id, int id_enfant, int id_utilisateur) {
		super();
		this.id = id;
		this.id_enfant = id_enfant;
		this.id_utilisateur = id_utilisateur;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId_enfant() {
		return id_enfant;
	}

	public void setId_enfant(int id_enfant) {
		this.id_enfant = id_enfant;
	}

	public int getId_utilisateur() {
		return id_utilisateur;
	}

	public void setId_utilisateur(int id_utilisateur) {
		this.id_utilisateur = id_utilisateur;
	}
}
