package projet.commun.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class DtoUtilisateur implements Serializable {

	private int id;

	private String nom;

	private String prenom;

	private String modePaiement;

	private String nombrePaiement;

	private String restantAPayer;

	public DtoUtilisateur() {
		super();
	}

	public DtoUtilisateur(int id, String nom, String prenom, String modePaiement, String nombrePaiement,
			String restantAPayer) {
		super();
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
		this.modePaiement = modePaiement;
		this.nombrePaiement = nombrePaiement;
		this.restantAPayer = restantAPayer;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getModePaiement() {
		return modePaiement;
	}

	public void setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
	}

	public String getNombrePaiement() {
		return nombrePaiement;
	}

	public void setNombrePaiement(String nombrePaiement) {
		this.nombrePaiement = nombrePaiement;
	}

	public String getRestantAPayer() {
		return restantAPayer;
	}

	public void setRestantAPayer(String restantAPayer) {
		this.restantAPayer = restantAPayer;
	}

}
