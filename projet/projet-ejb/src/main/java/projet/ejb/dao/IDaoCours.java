package projet.ejb.dao;

import java.util.List;

import projet.ejb.data.Cours;


public interface IDaoCours {

	int			inserer( Cours cours );

	void 		modifier( Cours cours );

	void 		supprimer( int idCours );

	Cours 		retrouver( int idCours );

	List<Cours> listerTout();
}
