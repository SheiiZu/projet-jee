package projet.ejb.dao;

import java.util.List;

import projet.ejb.data.Gestionnaire;


public interface IDaoGestionnaire {

	int			inserer( Gestionnaire gestionnaire );

	void 		modifier( Gestionnaire gestionnaire );

	void 		supprimer( int idGestionnaire );

	Gestionnaire 		retrouver( int idGestionnaire );

	List<Gestionnaire> listerTout();
}
