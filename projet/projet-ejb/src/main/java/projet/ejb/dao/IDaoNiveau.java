package projet.ejb.dao;

import java.util.List;

import projet.ejb.data.Niveau;


public interface IDaoNiveau {

	int			inserer( Niveau niveau );

	void 		modifier( Niveau niveau );

	void 		supprimer( int idNiveau );

	Niveau 		retrouver( int idNiveau );

	List<Niveau> listerTout();
}
