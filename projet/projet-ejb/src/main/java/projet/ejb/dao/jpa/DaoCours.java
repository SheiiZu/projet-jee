package projet.ejb.dao.jpa;

import static javax.ejb.TransactionAttributeType.MANDATORY;
import static javax.ejb.TransactionAttributeType.NOT_SUPPORTED;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import projet.ejb.dao.IDaoCours;
import projet.ejb.data.Cours;

@Stateless
@Local
@TransactionAttribute( MANDATORY )
public class DaoCours implements IDaoCours {

	
	// Champs
	@PersistenceContext
	private EntityManager	em;
	
	// Actions
	
	@Override
	public int inserer(Cours cours) {
		em.persist(cours);
		em.flush();
		return cours.getId();
	}

	@Override
	public void modifier(Cours cours) {
		em.merge( cours );
	}

	@Override
	public void supprimer(int idCours) {
		em.remove( retrouver(idCours) );
	}

	@Override
	@TransactionAttribute( NOT_SUPPORTED )
	public Cours retrouver(int idCours) {
		return em.find( Cours.class, idCours );
	}

	@Override
	@TransactionAttribute( NOT_SUPPORTED )
	public List<Cours> listerTout() {
		em.clear();
		var jpql = "SELECT c FROM Cours c ORDER BY c.libelle";
		var query = em.createQuery( jpql, Cours.class );
		return query.getResultList();
	}
}
