package projet.ejb.dao.jpa;

import static javax.ejb.TransactionAttributeType.MANDATORY;
import static javax.ejb.TransactionAttributeType.NOT_SUPPORTED;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import projet.ejb.dao.IDaoGestionnaire;
import projet.ejb.data.Gestionnaire;

@Stateless
@Local
@TransactionAttribute( MANDATORY )
public class DaoGestionnaire implements IDaoGestionnaire {

	
	// Champs
	
	@PersistenceContext
	private EntityManager	em;
	
	
	// Actions
	
	@Override
	public int inserer(Gestionnaire gestionnaire) {
		em.persist(gestionnaire);
		em.flush();
		return gestionnaire.getId();
	}

	@Override
	public void modifier(Gestionnaire gestionnaire) {
		em.merge( gestionnaire );
	}

	@Override
	public void supprimer(int idGestionnaire) {
		em.remove( retrouver(idGestionnaire) );
	}

	@Override
	@TransactionAttribute( NOT_SUPPORTED )
	public Gestionnaire retrouver(int idGestionnaire) {
		return em.find( Gestionnaire.class, idGestionnaire );
	}

	@Override
	@TransactionAttribute( NOT_SUPPORTED )
	public List<Gestionnaire> listerTout() {
		em.clear();
		var jpql = "SELECT c FROM Gestionnaire c ORDER BY c.ID_GESTIONNAIRE";
		var query = em.createQuery( jpql, Gestionnaire.class );
		return query.getResultList();
	}	
}
