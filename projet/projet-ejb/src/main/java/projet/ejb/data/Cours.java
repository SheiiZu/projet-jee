package projet.ejb.data;

import static javax.persistence.GenerationType.IDENTITY;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "cours")
public class Cours {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_COURS")
	private int id;
	
	@JoinColumn(name = "FK_PARENT",
			foreignKey = @ForeignKey(name = "FK_PARENT"))
	@Column(name = "FK_PARENT")
	private int fk_parent;
	
	@Column(name = "libelle")
	private String libelle;
	
	@Column(name = "dateHeureSeance")
	private LocalDateTime dateHeureSeance;
	
	@Column(name = "salle")
	private String salle;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public LocalDateTime getDateHeureSeance() {
		return dateHeureSeance;
	}

	public void setDateHeureSeance(LocalDateTime dateHeureSeance) {
		this.dateHeureSeance = dateHeureSeance;
	}

	public String getSalle() {
		return salle;
	}

	public void setSalle(String salle) {
		this.salle = salle;
	}

	public int getFk_parent() {
		return fk_parent;
	}

	public void setFk_parent(int fk_parent) {
		this.fk_parent = fk_parent;
	}
	
	
}
