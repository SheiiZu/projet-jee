package projet.ejb.data;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "enfant")
public class Enfant {

	@Column(name = "dateNaissance")
	private Date dateNaissance;

	@Column(name = "etablissement")
	private String etablissement;

	@Id
//	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id_enfant")
	private Integer id;

	@Column(name = "nom")
	private String nom;

	@Column(name = "prenom")
	private String prenom;

	public Enfant() {
		super();
	}

	public Enfant(Date dateNaissance, String etablissement, Integer id, String nom, String prenom) {
		super();
		this.dateNaissance = dateNaissance;
		this.etablissement = etablissement;
		this.id = id;
		this.nom = nom;
		this.prenom = prenom;
	}

	public Date getDateNaissance() {
		return dateNaissance;
	}

	public String getEtablissement() {
		return etablissement;
	}

	public Integer getId() {
		return id;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public void setEtablissement(String etablissement) {
		this.etablissement = etablissement;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
}
