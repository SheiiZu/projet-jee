package projet.ejb.data;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "gestionnaire")
public class Gestionnaire {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_GESTIONNAIRE")
	private int id;
	
	@JoinColumn(name = "FK_UTILISATEUR",
			foreignKey = @ForeignKey(name = "FK_UTILISATEUR"))
	@Column(name = "FK_UTILISATEUR")
	private int fk_utilisateur;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFk_utilisateur() {
		return fk_utilisateur;
	}

	public void setFk_utilisateur(int fk_utilisateur) {
		this.fk_utilisateur = fk_utilisateur;
	}
}
