package projet.ejb.data;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mouvement")
public class Mouvement {

	@Column(name = "dateMvmtCompteAsso")
	private Date dateMvmtCompteAsso;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_MOUVEMENT")
	private int id;

	public Date getDateMvmtCompteAsso() {
		return dateMvmtCompteAsso;
	}

	public void setDateMvmtCompteAsso(Date dateMvmtCompteAsso) {
		this.dateMvmtCompteAsso = dateMvmtCompteAsso;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
