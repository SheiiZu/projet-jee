package projet.ejb.data;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "niveau")
public class Niveau {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_NIVEAU")
	private int id;
	
	@Column(name = "LIBELLE")
	private int libelle;
	
	@JoinColumn(name = "FK_COURS",
			foreignKey = @ForeignKey(name = "FK_COURS"))
	@Column(name = "FK_COURS")
	private int fk_cours;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getLibelle() {
		return libelle;
	}

	public void setLibelle(int libelle) {
		this.libelle = libelle;
	}

	public int getFk_cours() {
		return fk_cours;
	}

	public void setFk_cours(int fk_cours) {
		this.fk_cours = fk_cours;
	}
}
