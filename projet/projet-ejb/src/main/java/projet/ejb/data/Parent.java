package projet.ejb.data;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = "parent")
public class Parent {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_PARENT")
	private int id;
	
	@JoinColumn(name = "FK_ENFANT",
			foreignKey = @ForeignKey(name = "FK_ENFANT"))
	@Column(name = "FK_ENFANT")
	private int fk_enfant;
	
	@JoinColumn(name = "FK_UTILISATEUR",
			foreignKey = @ForeignKey(name = "FK_UTILISATEUR"))
	@Column(name = "FK_UTILISATEUR")
	private int fk_utilisateur;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFk_utilisateur() {
		return fk_utilisateur;
	}

	public void setFk_utilisateur(int id_utilisateur) {
		this.fk_utilisateur = id_utilisateur;
	}

	public int getFk_enfant() {
		return fk_enfant;
	}

	public void setFk_enfant(int fk_enfant) {
		this.fk_enfant = fk_enfant;
	}
}
