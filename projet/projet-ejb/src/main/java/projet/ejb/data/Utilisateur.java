package projet.ejb.data;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "utilisateur")
public class Utilisateur {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID_UTILISATEUR")
	private int id;
	
	@Column(name = "nom")
	private String nom;
	
	@Column(name = "prenom")
	private String prenom;
	
	@Column(name = "modePaiement")
	private String modePaiement;
	
	@Column(name = "nombrePaiement")
	private String nombrePaiement;
	
	@Column(name = "restantAPayer")
	private String restantAPayer;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getModePaiement() {
		return modePaiement;
	}

	public void setModePaiement(String modePaiement) {
		this.modePaiement = modePaiement;
	}

	public String getNombrePaiement() {
		return nombrePaiement;
	}

	public void setNombrePaiement(String nombrePaiement) {
		this.nombrePaiement = nombrePaiement;
	}

	public String getRestantAPayer() {
		return restantAPayer;
	}

	public void setRestantAPayer(String restantAPayer) {
		this.restantAPayer = restantAPayer;
	}
	
	


}
