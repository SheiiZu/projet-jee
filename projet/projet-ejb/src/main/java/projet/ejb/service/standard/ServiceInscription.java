package projet.ejb.service.standard;

import static javax.ejb.TransactionAttributeType.NOT_SUPPORTED;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.inject.Inject;

import projet.commun.dto.DtoEnfant;
import projet.commun.exception.ExceptionValidation;
import projet.commun.service.IServiceEnfant;
import projet.ejb.dao.IDaoEnfant;
import projet.ejb.data.Enfant;
import projet.ejb.data.mapper.IMapperEjb;

@Stateless
@Remote
public class ServiceInscription implements IServiceEnfant {

	// Champs
	@Inject
	private IMapperEjb mapper;
	@Inject
	private IDaoEnfant daoEnfant;

	// Actions

	@Override
	public int inserer(DtoEnfant dtoEnfant) throws ExceptionValidation {
		verifierValiditeDonnees(dtoEnfant);
		int id = daoEnfant.inserer(mapper.map(dtoEnfant));
		return id;
	}

	@Override
	public void modifier(DtoEnfant dtoEnfant) throws ExceptionValidation {
		verifierValiditeDonnees(dtoEnfant);
		daoEnfant.modifier(mapper.map(dtoEnfant));
	}

	@Override
	public void supprimer(int idEnfant) throws ExceptionValidation {
		daoEnfant.supprimer(idEnfant);
	}

	@Override
	@TransactionAttribute(NOT_SUPPORTED)
	public DtoEnfant retrouver(int idEnfant) {
		return mapper.map(daoEnfant.retrouver(idEnfant));
	}

	@Override
	@TransactionAttribute(NOT_SUPPORTED)
	public List<DtoEnfant> listerTout() {
		List<DtoEnfant> liste = new ArrayList<>();
		for (Enfant enfant : daoEnfant.listerTout()) {
			liste.add(mapper.map(enfant));
		}
		return liste;
	}

	// Méthodes auxiliaires

	private void verifierValiditeDonnees(DtoEnfant dtoEnfant) throws ExceptionValidation {

		StringBuilder message = new StringBuilder();

		if (dtoEnfant.getNom() == null || dtoEnfant.getNom().isEmpty()) {
			message.append("\nLe nom est absent.");
			message.append("\nLe nom est trop court.");
		} else if (dtoEnfant.getNom().length() > 25) {
			message.append("\nLe nom est trop long.");
		}

		if (dtoEnfant.getPrenom() == null || dtoEnfant.getPrenom().isEmpty()) {
			message.append("\nLe prénom est absent.");
		} else if (dtoEnfant.getPrenom().length() < 3) {
			message.append("\nLe prénom est trop court.");
		} else if (dtoEnfant.getPrenom().length() > 25) {
			message.append("\nLe prénom est trop long.");
		}

		if (dtoEnfant.getDateNaissance() == null || dtoEnfant.getDateNaissance() == null) {
			message.append("\nLa date de naissance est absente.");
		}

		if (message.length() > 0) {
			throw new ExceptionValidation(message.toString().substring(1));
		}
	}

}
